#include<iostream>
#include<vector>

using namespace std;

class Engine
{
    private:
        void displayVector(vector< vector<bool> > vectorMatrix)
        {
            int outerSize = (int)(sizeof(vectorMatrix));
            int innerSize = (int)(sizeof(vectorMatrix[0]));
            for(int i = 0 ; i <= outerSize ; i++)
            {
                for(int j = 0 ; i < innerSize ; j++)
                {
                    cout<<vectorMatrix[i][j]<<" ";
                }
                cout<<endl;
            }
        }
    
	   vector< vector<bool> > initializeVector(int size , int reqSum)
	   {
           vector< vector<bool> > vectorMatrix;
           for(int i = 0 ; i < size ; i++)
           {
               vector<bool> innerMatrix;
               for(int j = 0 ; j <= reqSum ; j++)
               {
                   if(!j)
                   {
                       innerMatrix.push_back(true);
                   }
                   else
                   {
                       innerMatrix.push_back(false);
                   }
               }
               vectorMatrix.push_back(innerMatrix);
           }
//           displayVector(vectorMatrix);
           return vectorMatrix;
       }
    
	   bool checkIfSubsetExistsOrNot(vector< vector<bool> > vectorMatrix , vector<int> vectorData , int reqSum)
	   {
           int outerSize = (int)(sizeof(vectorData)/(int)sizeof(vectorData[0])) - 1;
           int innerSize = reqSum;
           for(int i = 0 ; i < outerSize ; i++)
           {
               for(int j = 1 ; j <= innerSize ; j++)
               {
                   if(i)
                   {
                       if(j < vectorData[i])
                       {
                           vectorMatrix[i][j] = vectorMatrix[i-1][j];
                       }
                       else if (j == vectorData[i])
                       {
                           vectorMatrix[i][j] = true;
                       }
                       else if(vectorMatrix[i-1][j] || vectorMatrix[i-1][j-vectorData[i]])
                       {
                           vectorMatrix[i][j] = true;
                       }
                   }
                   else
                   {
                       vectorMatrix[i][j] = j == vectorData[i] ? true : false;
                   }
               }
           }
           return vectorMatrix[outerSize-1][innerSize];
       }
    
    public:
        bool findSolution(vector<int> arrayDataVector , int requiredSum)
        {
            vector< vector<bool> > vectorMatrix = initializeVector((int)(sizeof(arrayDataVector)/sizeof(arrayDataVector[0])) - 1 , requiredSum);
            return checkIfSubsetExistsOrNot(vectorMatrix , arrayDataVector , requiredSum);
        }
};

int main()
{
    vector<int> arrayDataVector = {1 , 4 , 5 , 7 , 8};
    Engine      e               = Engine();
    cout<<e.findSolution(arrayDataVector , 11)<<endl;;
}
